[![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url]
[![LinkedIn][linkedin-shield]][linkedin-url]

<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://github.com/othneildrew/Best-README-Template">
    <img src="IMG_DEMO_FOR_GIT/logo2.png" alt="Logo" width="100%" height="auto">
  </a>

  <h3 align="center">Website Thương Mại Thời Trang Bán Giày</h3>

  <p align="center">
    Truy cập!
    <br />
    <a href="https://gitlab.com/l-m-thu-n/selling-shoes"><strong>Click ngay đọc dos »</strong></a>
    <br />
    <br />
    <a href="https://bangiay.tk">View Demo Website</a>
  </p>
</p>
<!-- ABOUT THE PROJECT -->
Để cài đặt Laravel 5.8 cần 1 số yêu cầu:

- PHP >= 7.3
- BCMath PHP Extension
- Ctype PHP Extension
- Fileinfo PHP Extension
- JSON PHP Extension
- Mbstring PHP Extension
- OpenSSL PHP Extension
- PDO PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension

Laravel thuộc sở hữu của [Composer](https://getcomposer.org/). Vì vậy trước khi cài laravel cần cài Composer trên máy tính trước 

### Built With

Tài Nguyên Sử Dụng
* [Bootstrap](https://getbootstrap.com)
* [JQuery](https://jquery.com)
* [Laravel](https://laravel.com)



<!-- GETTING STARTED -->
## Getting Started

This is an example of how you may give instructions on setting up your project locally.
To get a local copy up and running follow these simple example steps.

### hướng dẫn chạy lệnh

This is an example of how to list things you need to use the software and how to install them.
* Composer
  ```sh
  composer i
  ```

### Bước then chốt

1. created .env
   ```sh
   php artisan .env.example .env
   ```
2. created key
   ```sh
   php artisan key:generate
   ```
3. created databaes`
    ```sh
   php artisan migrate
   ```
## hướng dẫn Deploy Vps

sử dụng Vps + domain : 
* [Vultr](https://www.vultr.com/promo/try50/?service=try50&gclid=Cj0KCQiA5bz-BRD-ARIsABjT4ngAZAPi40_MO9VGrzTRTiV2q0PxVgSxKAYjTqiMmOEkPivX0OEWrcUaAr0REALw_wcB)
* [Freenom](https://www.freenom.com/vi/index.html?lang=vi)

### hướng dẫn setup server

tài liệu tham khảo để có thể cài đặt được server vps tại : * [Codezi](https://codezi.pro/category/web-server/nginx)
<!-- USAGE EXAMPLES -->

<!-- CONTRIBUTING -->
## Sử dụng git
1. Clone Project (`git clone https://gitlab.com/l-m-thu-n/selling-shoes.git`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin master`)
5. Open a Pull Request



<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.



<!-- CONTACT -->
## Contact

Your Name - [@Info-Me](https://flankos.tk/) - Email: Doanphuxm01@gmail.com

Project Link: [https://gitlab.com/l-m-thu-n/selling-shoes](https://gitlab.com/l-m-thu-n/selling-shoes)



<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements
* [GitHub Emoji Cheat Sheet](https://www.webpagefx.com/tools/emoji-cheat-sheet)
* [Img Shields](https://shields.io)
* [Choose an Open Source License](https://choosealicense.com)
* [GitHub Pages](https://pages.github.com)
* [Animate.css](https://daneden.github.io/animate.css)
* [Loaders.css](https://connoratherton.com/loaders)
* [Slick Carousel](https://kenwheeler.github.io/slick)
* [Smooth Scroll](https://github.com/cferdinandi/smooth-scroll)
* [Sticky Kit](http://leafo.net/sticky-kit)
* [JVectorMap](http://jvectormap.com)
* [Font Awesome](https://fontawesome.com)





<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/github/contributors/othneildrew/Best-README-Template.svg?style=for-the-badge
[contributors-url]: https://github.com/othneildrew/Best-README-Template/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/othneildrew/Best-README-Template.svg?style=for-the-badge
[forks-url]: https://github.com/othneildrew/Best-README-Template/network/members
[stars-shield]: https://img.shields.io/github/stars/othneildrew/Best-README-Template.svg?style=for-the-badge
[stars-url]: https://github.com/othneildrew/Best-README-Template/stargazers
[issues-shield]: https://img.shields.io/github/issues/othneildrew/Best-README-Template.svg?style=for-the-badge
[issues-url]: https://github.com/othneildrew/Best-README-Template/issues
[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=for-the-badge
[license-url]: https://github.com/othneildrew/Best-README-Template/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/othneildrew
[product-screenshot]: images/screenshot.png
